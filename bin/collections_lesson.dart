void main() {
  List<int> myList = [1, 2, 3];
  final myList2 = [1, 2, 3];
  <int>[1, 2, 3];
  [1, 2, 3];
  print(myList.length);
  // indexing - position
  final mySecondElement = myList[0];
  print(mySecondElement);

  Map<String, dynamic> myMap = {
    'name': 'John Doe',
    'age': 23,
    'registered': true,
  };
  final name = myMap['name'];

  // one instance of a certain value
  Set<int> mySet = {1, 2, 3, 2};
  print(mySet.length);

  final names = ['John', 'Jane', 'Mathew'];
  final nameLengths = names.map((name) => name.length).toList();
  print(nameLengths);

  final namesFiltered = names.where((name) => name.length == 4).toList();
  print(namesFiltered);

  for (int i = 0; i < namesFiltered.length; i++) {
    print(namesFiltered[i]);
  }

  // for each loop
  for (final name in namesFiltered) {
    print(name);
  }

  namesFiltered.forEach((name) {
    print(name);
  });

  namesFiltered.forEach((name) => print(name));

  bool isSignedIn = true;
  <String>[
    'This is fake content',
    if (isSignedIn) 'Sign Out' else 'Sign In',
  ];

  final x = <String>[
    for (int i = 0; i < 5; i++) i.toString(),
    for (final number in [1, 2, 3]) number.toString(),
  ];

  print(x);

  final list1 = ['hello', 'there'];
  final list2 = ['what\'s', 'up'];

  <String>[
    ...list1,
    ...list2,
  ];
}
