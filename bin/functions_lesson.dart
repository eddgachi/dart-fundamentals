void main() {
  // void means our function doesn't return anything
  void nestedFunction() {
    print('This is an example of a nested function');
  }

  nestedFunction(); // this is how we call a function in dart
  topLevelFunction();

  addNumbers(6, 1); // 6 & 7 are the arguments of a function
}

void topLevelFunction() {
  print('This is an example of a Top Level function');
}

addNumbers(int x, int y) {
  // x & y are the parameters of a function
  int z = x + y;
  print(z);
  return z;
}

void positionalParams(int x, double y, String greeting) {
  positionalParams(5, 3.5, 'Hello');
}

// // void optionalPositionalParams(int x, double y, [String greeting = 'Hi']) {
void optionalPositionalParams(int x, double y, [String? greeting]) {
  optionalPositionalParams(5, 3.5, 'Hi optionally');
}

void namedOptionalParams({
  int? x,
  double? y,
  String? greeting,
}) {
  namedOptionalParams(y: 5.5, x: 6);
}

void nameRequiredParams(
  int positional, {
  required int x,
  required double y,
  required String greeting,
}) {
  nameRequiredParams(5, x: x, y: y, greeting: greeting);
}
