void main() {
  String myString = 'Hello, World!';
  // var mySecondString = 'Hello World 2';
  // final mySecondString = 'Hello World 2';
  // mySecondString = 'Hello World 3';
  // int myInt = 5;
  // double myDouble = 5.5;
  // dynamic myVar = 9.0;
  // var result = 5 % 5; // remainder
  // var result = 5 / 5; // result
  // var result = 5 ~/ 5; // truncating division operator
  // double x = 5;
  // x = x + 5;
  // x++; //adds one to x x = x + 1;
  // x--; //subtracts one to x x = x - 1;
  // x+=5; //adds 5 to x x = x + 5;
  // x*=5; //adds 5 to x x = x * 5;
  // x/=5; //adds 5 to x x = x / 5;
  // bool isEqual = 5 != 10; // <, >, <=, >=,
  // int y = 8;

  // print('You have ' + y.toString() + ' items in your cart.');
  // print('You have  ${y + 5} items in your cart.');
}
