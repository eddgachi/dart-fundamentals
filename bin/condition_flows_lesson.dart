void main() {
  int myInteger = 5;

  if (myInteger == 10) {
    print('It\'s ten');
  } else if (myInteger == 9) {
    print('It\'s nine');
  } else if (myInteger > 20) {
    print('It\'s greater than twenty');
  } else {
    print('The integer is not ten, nine or greater than twenty');
  }

  switch (myInteger) {
    case 10:
      print('It\'s ten');
      break;
    case 9:
      print('It\'s nine');
      break;
    default:
      print('Oh! It\s something else');
  }
}
