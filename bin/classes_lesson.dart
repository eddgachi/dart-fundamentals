void main() {
  // print('Hello, World');

  String myString = 'Hello, World';
  var x = 4;

  // User myUser = User();
  // User myUser1 = User(
  //     firstName: 'John',
  //     lastName: 'Doe',
  //     photoUrl: 'http://example.com/profile.jpg');
  // User(name: 'John Doe', photoUrl: 'http://example.com/profile.jpg');
  // final myUser2 =
  //     User(name: 'Jane Doe', photoUrl: 'http://example.com/profile.jpg');
  // myUser2.name = 'Jane Doe';
  // print(myUser2.name);
  // User myUser3 =
  //     const User(name: 'John', photoUrl: 'http://example.com/profile.jpg');

  // User myUser4 =
  //     const User(name: 'John', photoUrl: 'http://example.com/profile.jpg');

  // print(myUser3 == myUser4);

  // myUser3.hasLongName();

  // User(name: 'a', photoUrl: 'b').hasLongName();

  // User.myMethod();
  // User.minNameLength;

  final p = Example(4, 9);
  print(p._private);
}

// class User {
//   String name = 'John Doe'; // fields - variable within a class
//   String photoUrl = 'http://example.com/profile.jpg';
// }

// class User {
//   String name;
//   String photoUrl;

//   User({
//     required this.name,
//     required this.photoUrl,
//   });
// }

class User {
  String name;
  String photoUrl;

  User({
    required String firstName,
    required String lastName,
    required this.photoUrl,
  }) : name = '$firstName + $lastName';
}

// class User {
//   final String name;
//   final String photoUrl;

//   const User({
//     required this.name,
//     required this.photoUrl,
//   });

//   bool hasLongName() {
//     return name.length > 10;
//   }

//   static void myMethod() {}

//   static const minNameLength = 3;
// }

class Example {
  int public;
  int _private;

  Example(this.public, this._private);

  Example.namedConstructor({
    required this.public,
    required int PrivateParameter,
  }) : _private = PrivateParameter;
}
